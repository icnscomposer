//
// AppDelegate.h
// Icns Composer
// https://github.com/raphaelhanneken/icnscomposer
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate> {
}

- (void) applicationDidFinishLaunching:(NSNotification *) n;

	// Reopen mainWindow, when the user clicks on the dock icon.
// - (BOOL) applicationShouldHandleReopen:(NSApplication *) app visibility:(BOOL) hasVisibleWindows;

- (void) applicationWillTerminate:(NSNotification *) n;

@end

//
// AppDelegate.m
// Icns Composer
// https://github.com/raphaelhanneken/icnscomposer
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void) applicationDidFinishLaunching:(NSNotification *) n;
{
	// Inser code here to set up your application
}

- (void) applicationWillTerminate:(NSNotification *) n;
{
	// Insert code here to tear down your application
}

@end

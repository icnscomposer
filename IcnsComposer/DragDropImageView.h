//
// DragAndDropImageView.h
// Icns Composer
// https://github.com/raphaelhanneken/icnscomposer
//

#import <Cocoa/Cocoa.h>

@interface DragDropImageView : NSImageView <NSDraggingSource>
{

    // Holds the last mouse down event, to track the drag distance.
	NSEvent *mouseDownEvent;
}

    // MARK: - NSDraggingSource

    // Since we only want to copy/delete the current image we register ourselfes
    // for .Copy and .Delete operations.
//    func draggingSession(_: NSDraggingSession, sourceOperationMaskFor _: NSDraggingContext) -> NSDragOperation {

- (NSDragOperation) draggingSession:(NSDraggingSession *) session sourceOperationMaskForDraggingContext:(NSDraggingContext)context;

    // Clear the ImageView on delete operation; e.g. the image gets
    // dropped on the trash can in the dock.
//    func draggingSession(_: NSDraggingSession, endedAt _: NSPoint, operation: NSDragOperation) {

- (void) draggingSession:(NSDraggingSession *) session movedToPoint:(NSPoint) screenPoint;

- (void) draggingSession:(NSDraggingSession *)session endedAtPoint:(NSPoint)screenPoint operation:(NSDragOperation)operation;

@end

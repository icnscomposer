//
// DragAndDropImageView.m
// Icns Composer
// https://github.com/raphaelhanneken/icnscomposer
//
#import "DragDropImageView.h"
#import "NSImageExtensions.h"

@implementation DragDropImageView

- (id) initWithFrame:(NSRect) frame;
{
	if((self=[super initWithFrame:frame]))
		{
        // Assure editable is set to true, to enable drop capabilities.
		[self setEditable:YES];
		}
	return self;
}

- (id) initWithCoder:(NSCoder *) coder;
{
	if((self=[super initWithCoder:coder]))
		{
        // Assure editable is set to true, to enable drop capabilities.
		[self setEditable:YES];
		}
	return self;
}

// MARK: - NSDraggingSource

// Since we only want to copy/delete the current image we register ourselfes
// for .Copy and .Delete operations.
- (NSDragOperation) draggingSession:(NSDraggingSession *) session sourceOperationMaskForDraggingContext:(NSDraggingContext) context;
{
	return NSDragOperationCopy | NSDragOperationDelete;
}

- (void) draggingSession:(NSDraggingSession *) session movedToPoint:(NSPoint) screenPoint;
{

}

// FIXME: we should also be able to select&delete without dragging to the trash can
- (void) draggingSession:(NSDraggingSession *) session endedAtPoint:(NSPoint) screenPoint operation:(NSDragOperation) operation;
{
	// Clear the ImageView on delete operation; e.g. the image gets
	// dropped on the trash can in the dock.
	if(operation & NSDragOperationDelete)
		[self setImage:nil];
}

// Track mouse down events and safe the to the poperty.
- (void) mouseDown:(NSEvent *) event
{
	[mouseDownEvent release];
	mouseDownEvent = [event retain];
}

// Track mouse dragged events to handle dragging sessions.
- (void) mouseDragged:(NSEvent *) event
{
	if (!mouseDownEvent)
		return;

	NSImage *image=[self image];
	if (!image)
		return;	// no image

	// Calculate the drag distance...
	NSPoint mouseDown = [mouseDownEvent locationInWindow];
	NSPoint dragPoint = [event locationInWindow];
	double dragDistance = hypot(mouseDown.x - dragPoint.x, mouseDown.y - dragPoint.y);

	// ...to cancel the dragging session in case of accidental drag.
	if (dragDistance < 3)
		return;

	NSSize size = NSMakeSize(log10([image size].width) * 30, log10([image size].height) * 30);

	NSImage *img = [image resizeToSize:size];
	if (!img)
		return;	// no image

	// Create a new NSDraggingItem with the image as content.
	NSDraggingItem *draggingItem = [[NSDraggingItem alloc] initWithPasteboardWriter:image];
	// Calculate the mouseDown location from the window's coordinate system to the ImageViews
	// coordinate system, to use it as origin for the dragging frame.
	NSPoint draggingFrameOrigin = [self convertPoint:mouseDown fromView:nil];
	// Build the dragging frame and offset it by half the image size on each axis
	// to center the mouse cursor within the dragging frame.
	NSRect draggingFrame = (NSRect){draggingFrameOrigin, img.size};
	draggingFrame.origin.x += -img.size.width / 2;
	draggingFrame.origin.y += -img.size.height / 2;

	 // Assign the dragging frame to the draggingFrame property of our dragging item.
	[draggingItem setDraggingFrame:draggingFrame];

	#if MATERIAL
	// Provide the components of the dragging image.
	[self setImageComponentsProvider: array of single block]
		 draggingItem.imageComponentsProvider = {
			 let component      = NSDraggingImageComponent(key: NSDraggingItem.ImageComponentKey.icon)
			 component.contents = image
			 component.frame    = NSRect(origin: NSPoint(), size: draggingFrame.size)

			 return [component]
		 }

		 // Begin actual dragging session. Woohow!
		 beginDraggingSession(with: [draggingItem], event: mouseDownEvent!, source: self)
	 }
#endif
	[self beginDraggingSessionWithItems:[NSArray arrayWithObject:draggingItem] event:mouseDownEvent source:self];
	[mouseDownEvent release];
	mouseDownEvent = nil;	// processed
	[draggingItem release];
}

@end

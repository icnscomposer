//
// MainWindowController.h
// Icns Composer
// https://github.com/raphaelhanneken/icnscomposer
//

#import <Cocoa/Cocoa.h>
#import "Iconset.h"
#import "DragDropImageView.h"

/// Manages the MainWindow.xib.
@interface IcnsDocument : NSDocument <NSWindowDelegate>
{
	IBOutlet DragDropImageView *image_1024;
	IBOutlet DragDropImageView *image_512;
	IBOutlet DragDropImageView *image_256;
	IBOutlet DragDropImageView *image_128;
	IBOutlet DragDropImageView *image_64;
	IBOutlet DragDropImageView *image_32;
	IBOutlet DragDropImageView *image_16;
    /// Handles creation of the .icns file.
	IconSet *iconset;
}

    /// Starts the export process for the current iconset.
    ///
    /// - parameter sender: NSToolbarItem that sent the export message.
- (IBAction) export:(id) sender;

    /// Gets called everytime a user drops an image onto a connected NSImageView.
    /// Resizes the dropped images to the appropriate size and adds them to the icon object.
    ///
    /// - parameter sender: The NSImageView that the images have been assigned to.
- (IBAction) resize:(NSImageView *) sender;

@end

//
// MainWindowController.m
// Icns Composer
// https://github.com/raphaelhanneken/icnscomposer
//

#import "IcnsDocument.h"

/// Manages the MainWindow.xib.
@implementation IcnsDocument

    /// Handles creation of the .icns file.
- (id) init
{
	if((self = [super init]))
		iconset = [IconSet new];
	return self;
}

- (NSString *) windowNibName
{
	return @"MainWindow";
}

- (void) windowControllerDidLoadNib:(NSWindowController *) windowController
{ // install loaded images
	IconImage *img;
	img=[iconset iconImageForSize:NSMakeSize(16, 16)];
	if(img) [image_16 setImage:[img image]];
	img=[iconset iconImageForSize:NSMakeSize(32, 32)];
	if(img) [image_32 setImage:[img image]];
	img=[iconset iconImageForSize:NSMakeSize(64, 64)];
	if(img) [image_64 setImage:[img image]];
	img=[iconset iconImageForSize:NSMakeSize(128, 128)];
	if(img) [image_128 setImage:[img image]];
	img=[iconset iconImageForSize:NSMakeSize(256, 256)];
	if(img) [image_256 setImage:[img image]];
	img=[iconset iconImageForSize:NSMakeSize(512, 512)];
	if(img) [image_512 setImage:[img image]];
	img=[iconset iconImageForSize:NSMakeSize(1024, 1024)];
	if(img) [image_1024 setImage:[img image]];
}

- (BOOL) readFromData:(NSData *) data ofType:(NSString *) type error:(NSError **) error;
{
	// handle opening .icns or .(app)iconset
	if([type isEqualToString:@"Icns Document"])
		return [iconset loadFromData:data];
	if([type isEqualToString:@"IconSet Document"])
		;	// how can this happen? IconSet is a directory...
	return NO;
}

- (NSData *) dataOfType:(NSString *) type error:(NSError **) error;
{
	if([type isEqualToString:@"Icns Document"])
		return [iconset data];
	if([type isEqualToString:@"IconSet Document"])
		// FIXME: can't convert to NSData...
		;
	return nil;
}

    /// Starts the export process for the current iconset.
    ///
    /// - parameter sender: NSToolbarItem that sent the export message.
- (IBAction) export:(id) sender;
{
	NSSavePanel *dialog = [[NSSavePanel new] autorelease];
	[dialog setAllowedFileTypes:[NSArray arrayWithObject:@"icns"]];
	[dialog setAllowsOtherFileTypes:NO];
	if([dialog runModalForDirectory:nil file:nil] != NSModalResponseOK)
		return;

	if(![iconset writeToURL:[dialog URL]])
		{
		NSAlert *alert = [NSAlert new];
		[alert setMessageText:@"Oh snap!"];
		[alert setInformativeText:@"Something went somewhere terribly wrong."];
		[alert runModal];
		}
}

    /// Gets called everytime a user drops an image onto a connected NSImageView.
    /// Resizes the dropped images to the appropriate size and adds them to the icon object.
    ///
    /// - parameter sender: The NSImageView that the images have been assigned to.
- (IBAction) resize:(NSImageView *) sender;
{
	IconImage *img1 = [[IconImage alloc] initWithImage:[sender image] withSize:NSMakeSize(sender.tag / 2, sender.tag / 2) andScale:scale1x];
	IconImage *img2 = [[IconImage alloc] initWithImage:[sender image] withSize:NSMakeSize(sender.tag, sender.tag) andScale:scale2x];

	[iconset setIconImage:img1 forSubscript:[img1 filename]];
	[iconset setIconImage:img2 forSubscript:[img2 filename]];

	[img1 release];
	[img2 release];
	// mark document as dirty
}

@end

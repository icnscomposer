//
// IconImage.h
// Icns Composer
// https://github.com/raphaelhanneken/icnscomposer
//

#import <Cocoa/Cocoa.h>

/// The scales of an icon image.
///
/// - scale1x: An image scale of @1x
/// - scale2x: An image scale of @2x
//enum ImageScale: String {
//    case scale1x = "@1x"
//    case scale2x = "@2x"
//}

typedef NSString *ImageScale;
extern NSString *scale1x;
extern NSString *scale2x;

@interface IconImage : NSObject
{

    /// The image object.
	NSImage *image;

    /// The scale of the image object.
	ImageScale scale;

    /// The images size.
	NSSize size;
}


    /// The filename of the icon image based on it's size and scale, e.g. icon_32x32@2x.png
	// var filename: String {
- (NSString *) filename;
- (NSImage *) image;
- (NSSize) size;


    /// Initializes a new iconset image.
    ///
    /// - parameter image: The NSImage object, the IconImage should.
    /// - parameter size: The NSSize the supplied image should be resized to.
    /// - parameter scale: The scale type of the image.
//    init?(_ image: NSImage?, withSize size: NSSize, andScale scale: ImageScale) {

- (id) initWithImage:(NSImage *) image withSize:(NSSize) size andScale:(ImageScale) scale;

/// Writes the iconset image to the given url.
    ///
    /// - parameter url: The url to save the image to.
//    func writeToURL(_ url: URL) throws {
- (BOOL) writeToDirectoyURL:(NSURL *) url;

@end

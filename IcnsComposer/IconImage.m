//
// IconImage.m
// Icns Composer
// https://github.com/raphaelhanneken/icnscomposer
//

#import "IconImage.h"
#import "NSImageExtensions.h"

/// The scales of an icon image.
///
/// - scale1x: An image scale of @1x
/// - scale2x: An image scale of @2x

NSString *scale1x = @"@1x";
NSString *scale2x = @"@2x";

@implementation IconImage

- (NSString *) filename;
{ // default file name
	if([scale isEqualToString:scale1x])
		return [NSString stringWithFormat:@"icon_%dx%d%@.png", (int) size.width, (int) size.height, scale];
	if([scale isEqualToString:scale2x])
		return [NSString stringWithFormat:@"icon_%dx%d%@.png", (int) (size.width/2), (int) (size.height/2), scale];
	return @"unknown scale";
}

- (NSImage *) image; { return image; }
- (NSSize) size; { return size; }

    /// Initializes a new iconset image.
    ///
    /// - parameter image: The NSImage object, the IconImage should.
    /// - parameter size: The NSSize the supplied image should be resized to.
    /// - parameter scale: The scale type of the image.
- (id) initWithImage:(NSImage *) image withSize:(NSSize) size andScale:(ImageScale) scale;
{
	if(!image)
		{
		[self release];
		return nil;
		}
	if(self=[super init])
		{
		self->image = [[image resizeToSize:size] retain];
		self->scale = scale;
		self->size = size;
		}
	return self;
}

- (void) dealloc
{
	[image release];
	[super dealloc];
}

    /// Writes the iconset image to the given url.
    ///
    /// - parameter url: The url to save the image to.
- (BOOL) writeToDirectoyURL:(NSURL *) url;
{
	NSURL *imgURL = [url URLByAppendingPathComponent:[self filename]];

	NSData *png = [image PNGRepresentation];
	return [png writeToURL:imgURL atomically:NO];
}

@end

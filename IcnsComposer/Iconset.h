//
// Iconset.h
// Icns Composer
// https://github.com/raphaelhanneken/icnscomposer
//

#import <Cocoa/Cocoa.h>
#import "IconImage.h"

/// Error cases for icns file generation
///
/// - missingURL: The given URL, to save the icns file to, is missing.
//enum IconsetError: Error {
//    case missingURL
//}

extern NSString *missingURLException;

@interface IconSet : NSObject
{

    /// The necessary images to create an iconset that conforms iconutil
    // var images: [String: IconImage]
	NSMutableDictionary *images;
}

    /// Subscript to access a specific image inside the Iconset by it's filename.
    ///
    /// - parameter filename: The filename of the image to access
//    subscript(filename: String) -> IconImage? {

- (IconImage *) subscript:(NSString *) filename;
- (IconImage *) iconImageForSize:(NSSize) size;

- (void) setIconImage:(IconImage *) image forSubscript:(NSString *) filename;

    ///  Saves an icns file to the supplied url.
    ///
    ///  - parameter url: URL to save the icns file to.
    ///  - throws: A MissingURL error, when the supplied url can't be unwrapped.
//    func writeToURL(_ url: URL?) throws {
- (BOOL) loadFromData:(NSData *) data;
- (BOOL) writeToURL:(NSURL *) url;
- (NSData *) data;

    /// Create a new iconset within the user's temp directory.
    ///
    /// - returns: The URL where the iconset were written to.

// fileprivate func writeToTemporaryDir() throws -> URL {
- (NSURL *) writeToTemporaryDir;

    ///  Executes iconutil with the given url as input path.
    ///
    ///  - parameter input: Path to a convertable iconset directory.
    ///  - parameter output: Path to the location, where to save the generated icns file.
    ///  - throws: Throws a MissingURL error, in case one of the supplied urls cant be unwrapped.
//    private func runIconUtilWithInput(_ input: URL?, andOutputURL output: URL?) throws {
- (BOOL) runIconUtilWithInput:(NSURL *) input andOutput:(NSURL *) output;

@end

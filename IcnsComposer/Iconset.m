//
// Iconset.m
// Icns Composer
// https://github.com/raphaelhanneken/icnscomposer
//

#import "Iconset.h"

/// Error cases for icns file generation
///
/// - missingURL: The given URL, to save the icns file to, is missing.
NSString *missingURLException = @"missingURLException";

@implementation IconSet

/// Initializes a new Iconset
- (id) init;
{
	if((self=[super init]))
		{
		images=[NSMutableDictionary new];
		}
	return self;
}

- (void) dealloc;
{
	[images release];
	[super dealloc];
}

/// Subscript to access a specific image inside the Iconset by it's filename.
///
/// - parameter filename: The filename of the image to access

- (IconImage *) subscript:(NSString *) filename;
{
	return [images objectForKey:filename];
}

- (IconImage *) iconImageForSize:(NSSize) size;
{
	NSEnumerator *e=[images objectEnumerator];
	IconImage *img;
	while((img=[e nextObject]))
		{
		if(NSEqualSizes([img size], size))
			return img;
		}
	return nil;
}

- (void) setIconImage:(IconImage *) image forSubscript:(NSString *) filename;
{
	[images setObject:image forKey:filename];
}

- (BOOL) loadFromData:(NSData *) data
{
	NSURL *output=[NSURL fileURLWithPath:NSTemporaryDirectory()];
	NSURL *input=output;
	NSEnumerator *e;
	NSString *filename;
	output=[output URLByAppendingPathComponent:@"temp.iconset"];
	[[NSFileManager defaultManager] removeItemAtURL:output error:NULL];	// may still exist
	if(![[NSFileManager defaultManager] createDirectoryAtURL:output withIntermediateDirectories:YES attributes:nil error:NULL])
		return NO;
	input=[input URLByAppendingPathComponent:@"temp.icns"];
	if(![data writeToURL:input atomically:NO])
		return NO;
	if(![self runIconUtilWithInput:input andOutput:output])
		return NO;
	e=[[NSFileManager defaultManager] enumeratorAtPath:[output path]];
	while((filename=[e nextObject]))
		{
		NSURL *url=[output URLByAppendingPathComponent:filename];
		NSImage *img=[[NSImage alloc] initWithContentsOfURL:url];
		IconImage *image;
		double d;
		NSSize size;
		ImageScale scale;
		NSScanner *sc=[NSScanner scannerWithString:filename];
		if(!img)
			return NO;
		if(![sc scanString:@"icon_" intoString:NULL])
			return NO;
		if(![sc scanDouble:&d])
			return NO;
		size.height=d;
		if(![sc scanString:@"x" intoString:NULL])
			return NO;
		if(![sc scanDouble:&d])
			return NO;
		size.width=d;
		scale=scale1x;
		if([sc scanString:@"@2x." intoString:NULL])
			scale=scale2x;
		else if([sc scanString:@"@1x." intoString:NULL])
			scale=scale1x;
		if(![sc scanString:@"png" intoString:NULL])
			return NO;
		image=[[IconImage alloc] initWithImage:img withSize:size andScale:scale];
		if(!image)
			return NO;
		[self setIconImage:image forSubscript:filename];
		[image release];
		[img release];
		}
	// display
	return YES;
}

- (NSData *) data;
{
	NSURL *input=[self writeToTemporaryDir];	// write individual files to tmp dir
	NSURL *output=[NSURL fileURLWithPath:NSTemporaryDirectory()];
	output=[output URLByAppendingPathComponent:@"temp.icns"];
	if(![self runIconUtilWithInput:input andOutput:output])
		return nil;
	return [NSData dataWithContentsOfURL:output];
}

    ///  Saves an icns file to the supplied url.
    ///
    ///  - parameter url: URL to save the icns file to.
    ///  - throws: A MissingURL error, when the supplied url can't be unwrapped.

//    func writeToURL(_ url: URL?) throws {
- (BOOL) writeToURL:(NSURL *) url;
{
	NSURL *images=[self writeToTemporaryDir];	// write individual files to tmp dir
	return [self runIconUtilWithInput:images andOutput:url];
}
    /// Create a new iconset within the user's temp directory.
    ///
    /// - returns: The URL where the iconset were written to.

// fileprivate func writeToTemporaryDir() throws -> URL {
- (NSURL *) writeToTemporaryDir;
{
	NSURL *dir=[NSURL fileURLWithPath:NSTemporaryDirectory()];
	NSEnumerator *e=[images keyEnumerator];
	NSString *filename;
	dir=[dir URLByAppendingPathComponent:@"icns.iconset"];
	if(![[NSFileManager defaultManager] createDirectoryAtURL:dir withIntermediateDirectories:YES attributes:nil error:NULL])
		return nil;
	while((filename=[e nextObject]))
		{
		if(![[self subscript:filename] writeToDirectoyURL:dir])
			return nil;
		}
	return dir;
}

    ///  Executes iconutil with the given url as input path.
    ///
    ///  - parameter input: Path to a convertable iconset directory.
    ///  - parameter output: Path to the location, where to save the generated icns file.
    ///  - throws: Throws a MissingURL error, in case one of the supplied urls cant be unwrapped.
//    private func runIconUtilWithInput(_ input: URL?, andOutputURL output: URL?) throws {
- (BOOL) runIconUtilWithInput:(NSURL *) input andOutput:(NSURL *) output;
{
	NSTask *iconUtil = [[NSTask new] autorelease];
	[iconUtil setLaunchPath:@"/usr/bin/iconutil"];
//	[iconUtil setLaunchPath:@"/bin/echo"];
	[iconUtil setArguments:[NSArray arrayWithObjects:@"-c", [output pathExtension], @"-o", [output path], [input path], nil]];
	[iconUtil launch];
	[iconUtil waitUntilExit];
	// check exit value!

	return [[NSFileManager defaultManager] removeItemAtURL:input error:NULL];
}

@end

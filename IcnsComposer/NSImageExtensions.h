//
// NSImageExtensions.h
// Icns Composer
// https://github.com/raphaelhanneken/icnscomposer
//

#import <Cocoa/Cocoa.h>

/// Exceptions for the image extension class.
///
/// - creatingPngRepresentationFailed: Is thrown when the creation of the png representation failed.
//enum NSImageExtensionError: Error {
//    case creatingPngRepresentationFailed
//}

@interface NSImage (Extension)
{
}

- (CGFloat) height;
- (CGFloat) width;

- (NSData *) PNGRepresentation;

// Resizes the image to the given size.
//
// - parameter size: The size to resize the image to.
// - returns: The resized image.
- (NSImage *) resizeToSize:(NSSize) size;

// Copies the image and resizes it to the supplied size, while maintaining it's
// original aspect ratio.
//
// - parameter size: The target size of the image.
// - returns: The resized image.

- (NSImage *) resizeMaintainingAspectRatioWithSize:(NSSize) size;

// Resizes the image, to nearly fit the supplied cropping size
// and return a cropped copy the image.
//
// - parameter size: The size of the new image.
// - returns: The cropped image.
- (NSImage *) cropToSize:(NSSize) size;

// Saves the images PNG representation to the supplied file URL.
//
// - parameter url: The file URL to save the png file to.
// - throws: An NSImageExtension Error.
- (BOOL) savePngToURL:(NSURL *) url;

@end

//
// NSImageExtensions.m
// Icns Composer
// https://github.com/raphaelhanneken/icnscomposer
//

#import "NSImageExtensions.h"

@implementation NSImage (Extension)

- (CGFloat) height; { return [self size].height; }
- (CGFloat) width; { return [self size].width; }

- (NSData *) PNGRepresentation;
{
	NSSize size = [self size];

	NSBitmapImageRep *rep = [[NSBitmapImageRep alloc]
							 initWithBitmapDataPlanes:NULL
							 pixelsWide:size.width
							 pixelsHigh:size.height
							 bitsPerSample:8
							 samplesPerPixel:4
							 hasAlpha:YES
							 isPlanar:NO
							 colorSpaceName:NSCalibratedRGBColorSpace
							 bytesPerRow:0
							 bitsPerPixel:0];
	[NSGraphicsContext saveGraphicsState];
	[NSGraphicsContext setCurrentContext:[NSGraphicsContext graphicsContextWithBitmapImageRep:rep]];
	[self drawInRect:(NSRect){ NSZeroPoint, size } fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0];
	[NSGraphicsContext restoreGraphicsState];

	return [rep representationUsingType:NSPNGFileType properties:[NSDictionary dictionary]];
}

// Resizes the image to the given size.
//
		// - parameter size: The size to resize the imageTIFFRepresentation The resized image.
- (NSImage *) resizeToSize:(NSSize) size;
{
	NSRect frame = NSMakeRect(0, 0, size.width, size.height);

	NSImageRep *rep=[self bestRepresentationForRect:frame context:nil hints:nil];

	NSImage *img = [[NSImage alloc] initWithSize:size];
	[img lockFocus];
	[rep drawInRect:frame];
	[img unlockFocus];
	return [img autorelease];
}

// Copies the image and resizes it to the supplied size, while maintaining it's
// original aspect ratio.
//
// - parameter size: The target size of the image.
// - returns: The resized image.

- (NSImage *) resizeMaintainingAspectRatioWithSize:(NSSize) size;
{
	NSSize newSize;

	CGFloat widthRatio = size.width / [self width];
	CGFloat heightRatio = size.height / [self height];

    if (widthRatio > heightRatio) {
		newSize = NSMakeSize(floor([self width] * widthRatio),
						 floor([self height] * widthRatio));
	} else {
		newSize = NSMakeSize(floor([self width] * heightRatio),
							 floor([self height] * heightRatio));
	}

	return [self resizeToSize:newSize];
}

// Resizes the image, to nearly fit the supplied cropping size
// and return a cropped copy the image.
//
// - parameter size: The size of the new image.
// - returns: The cropped image.
- (NSImage *) cropToSize:(NSSize) size;
{
#if MATERIAL
			guard let resized = self.resizeMaintainingAspectRatio(withSize: size) else {
				return nil
			}

			// swiftlint:disable identifier_name
			let x = floor((resized.width - size.width) / 2)
			let y = floor((resized.height - size.height) / 2)
			let frame = NSRect(x: x, y: y, width: width, height: height)

			guard let rep = resized.bestRepresentation(for: frame, context: nil, hints: nil) else {
				return nil
			}

			let img = NSImage(size: size)
			defer { img.unlockFocus() }
			img.lockFocus()

			if rep.draw(in: NSRect(x: 0, y: 0, width: width, height: height),
						from: frame,
						operation: NSCompositingOperation.copy,
						fraction: 1.0,
						respectFlipped: false,
						hints: [:]) {
				return img
			}

			return nil
		}
#endif
	return self;
}

    /// Saves the images PNG representation to the supplied file URL.
    ///
    /// - parameter url: The file URL to save the png file to.
    /// - throws: An NSImageExtension Error.

- (BOOL) savePngToURL:(NSURL *) url;
{
	NSData *rep=[self PNGRepresentation];
	return [rep writeToURL:url atomically:YES];
}

@end
